import argparse
import os

import pandas

from fernet import Fernet

lazy_crypt_dict = {}

def get_key():
    key_file = 'encryption_key.bin'
    if os.path.exists(key_file):
        with open(key_file, 'rb') as fp:
            key = fp.read()
    else:
        key = Fernet.generate_key()
        with open(key_file, 'wb') as fp:
            fp.write(bytearray(key))
    
    return key
    
def encrypt(data_file):


    key = get_key()
    f = Fernet(key)
    
    # Pandas dataframe
    df = pandas.read_csv(data_file, low_memory=False)


    def _encrypt(string):
        string = str(string)
        if string not in lazy_crypt_dict.keys():
            cipher = f.encrypt(string).decode('utf-8')
            lazy_crypt_dict[string] = cipher
        return lazy_crypt_dict[string]

    ## Encrypt Site Column
    # df['Site'] = df['Site'].apply(_encrypt)
    
    # Encrypt InstitutionID Column
    df['InstitutionID'] = df['InstitutionID'].apply(_encrypt)
    
    # Create new column RunDataIDOBF (Obfuscated) with first 3 letters of RunDataID
    #df['RunDataIdObf'] = df['RunDataId'].apply(lambda x: x[:3])
    
    # Encrypt RunDataId column
    df['RunDataID'] = df['RunDataID'].apply(_encrypt)

    # Encrypt InstrumentSerialNumber column
    df['InstrumentSerialNumber'] = df['InstrumentSerialNumber'].apply(_encrypt)
    filename = os.path.basename(data_file)
    df.to_csv(os.path.join('encrypted', filename), index=False)


def decrypt(data_file):
    key = get_key()
    f = Fernet(key)

    df = pandas.read_csv(data_file)

    def _decrypt(string): 
        if string not in lazy_crypt_dict.keys():
            decipher = f.decrypt(string.encode()).decode('utf-8')
            lazy_crypt_dict[string] = decipher
        return lazy_crypt_dict[string]


    df['Site'] = df['Site'].apply(_decrypt)
    
    df['Institution'] = df['Institution'].apply(_decrypt)
    
    df['RunDataId'] = df['RunDataId'].apply(_decrypt)

    filename = os.path.basename(data_file)
    df.to_csv(os.path.join('decrypted', filename), index=False)




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('source_file', help="The csv to clean and encrypt")
    parser.add_argument('--decrypt', default=False, action='store_true')
    args = parser.parse_args()

    if args.decrypt:
        decrypt(args.source_file)
    else:
        encrypt(args.source_file)
