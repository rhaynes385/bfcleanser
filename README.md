## Requirements
```shell
git
python 3.6
virtualenv
```

## Initial setup

### Linux
```shell
virtualenv .bfcleanser
source .bfcleanse/bin/activate
pip install -r requirements.txt
```

### Windows
¯\\\_(ツ)\_/¯

## Usage
```shell
python clean_rp_data.py data/data_file
```
